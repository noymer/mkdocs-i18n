# Només català i castellà

Pàgina en un subdirectori i no està traduida al anglés en un subdirectori (Page in a subdirectory and not translated to English).

Veure <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L50>
