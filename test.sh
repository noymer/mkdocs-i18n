#!/bin/sh -ex
pip install -e .[test]
isort --check .
black --check .
flake8
pylint mkdocs_i18n spec/*.py *.py
bandit -r .
mamba --enable-coverage
coverage report --fail-under=100
pypi-version-check
